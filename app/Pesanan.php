<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $fillable = [
        'kode_pesanan',
        'status',
        'total_harga',
        'kode_baju',
        'user_id',
    ];

    public function pesanan_details()
    {
        return $this->hasMany(pesanandetail::class, 'pesanan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
