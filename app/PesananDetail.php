<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesananDetail extends Model
{
    protected $fillable = [
        'jumlah_pesanan',
        'total_harga',
        'nameset',
        'nama',
        'nomer',
        'product_id',
        'pesanan_id',

    ];

    public function pesanan()
    {
        return $this->belongsTo(pesanan::class, 'pesanan_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(product::class, 'product_id', 'id');
    }
}
