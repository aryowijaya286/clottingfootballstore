<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function liga()
    {
        return $this->belongsTo(liga::class, 'liga_id', 'id');
    }

    public function pesanan_details()
    {
        return $this->hasMany(pesanandetail::class, 'product_id', 'id');
    }
}
